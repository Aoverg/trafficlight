# Traffic light using an Arduino Uno

## Code used in project
```cpp
int car1R = 12;
int car1Y = 11;
int car1G = 10;
int walk1R = 9;
int walk1G = 8;
int walk2R = 7;
int walk2G = 6;
int car2R = 5;
int car2Y = 4;
int car2G = 3;

int waitForWalk = 1000;
int delayVal = 3000;

void setup() {
  pinMode(car1R, OUTPUT);
  pinMode(car1Y, OUTPUT);
  pinMode(car1G, OUTPUT);
  pinMode(car2R, OUTPUT);
  pinMode(car2Y, OUTPUT);
  pinMode(car2G, OUTPUT);
  pinMode(walk1R, OUTPUT);
  pinMode(walk2R, OUTPUT);
  pinMode(walk1G, OUTPUT);
  pinMode(walk2G, OUTPUT);
}

void loop() {
  for(int i = 0; i < 9; i++){
    switch(i){
      case 0: 
        digitalWrite(car1R, LOW);
        digitalWrite(car2R, LOW);
        digitalWrite(car1Y, LOW);
        digitalWrite(car2Y, LOW);
        digitalWrite(car1G, HIGH);
        digitalWrite(car2G, HIGH);
        delay(delayVal);
        break;
      case 1:
        digitalWrite(car1G, LOW);
        digitalWrite(car2G, LOW);
        digitalWrite(car1Y, HIGH);
        digitalWrite(car2Y, HIGH);
        delay(delayVal);
        break;
      case 2:
        digitalWrite(car1Y, LOW);
        digitalWrite(car2Y, LOW);
        digitalWrite(car1R, HIGH);
        digitalWrite(car2R, HIGH);
        delay(delayVal);
        break;
      case 3:
        delay(waitForWalk);
        break;
      case 4:
        digitalWrite(walk1R, LOW);
        digitalWrite(walk2R, LOW);
        digitalWrite(walk1G, HIGH);
        digitalWrite(walk2G, HIGH);
        delay(delayVal/2);
        break;
      case 5:
        for(int blink = 0; blink < 10; blink++){
          int toggel = digitalRead(walk1G);
          if(toggel == HIGH){
            digitalWrite(walk2G, LOW);
            digitalWrite(walk1G, LOW);
            delay(delayVal/10);
          }
          else if(toggel == LOW){
            digitalWrite(walk2G, HIGH);
            digitalWrite(walk1G, HIGH);
            delay(delayVal/10);
          }   
        }
        break;
      case 6:
        digitalWrite(walk1G, LOW);
        digitalWrite(walk2G, LOW);
        digitalWrite(walk1R, HIGH);
        digitalWrite(walk2R, HIGH);
        break;
      case 7:
        delay(waitForWalk);
        break;
      case 8:
        digitalWrite(car1Y, HIGH);
        digitalWrite(car2Y, HIGH);
        delay(delayVal);
        break;
      default:
        break;
    }
  }
}
```

![Screenshot](img_trafficlight.png)